﻿/* © 2015 Studio Pepwuper http://www.pepwuper.com */

using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour {

	public GameObject Cursor; // Google Cardboard SDK: Cursor / GazePointer from CardboardMain Prefab
	private Vector3 goal;
	private NavMeshAgent agent;
	public float x;
	public float y;
	public float z;
	public float d;
	public Vector3 a;
	public Vector3 p1, p2;
	void Start() {
		//CardboardGUI.onGUICallback += this.OnGUI;
		this.agent = GetComponent<NavMeshAgent>();
		this.goal = new Vector3(0f, 0f, 0f);
	}
	
	//Set navigation destination to the position of the cursor
	//Ex. Call this from an event trigger on the floor object
	public void SetDestinationToCursor() {
		this.goal = Cursor.transform.position;
		Debug.Log (Cursor.transform.position);
		MoveToDestination();
	}

	public void DetectObject(){
		Vector3 zero = new Vector3(0, 0, 0);
		if (p1 == zero) {
			p1 = Cursor.transform.position;
			Debug.Log ("p1 selected:" +p1);
		} else if (p2 == zero) {
			p2 = Cursor.transform.position;
			Debug.Log ("p2 selected:" +p2);
			d = Vector3.Distance (p1, p2);
		}	
	}

	void OnGUI() {
		//if (!CardboardGUI.OKToDraw(this)) return;
		//d =  Vector3.Distance(Cursor.transform.position, a);
		x = Mathf.Round((Cursor.transform.position.x) * 1000f) / 1000f;
		y = Mathf.Round((Cursor.transform.position.y) * 1000f) / 1000f;
		z = Mathf.Round((Cursor.transform.position.z) * 1000f) / 1000f;
		GUI.Label ((new Rect (0, 0, 200, 100)), "X coordinate : " +x);
		GUI.Label ((new Rect (0, 40, 200, 100)), "Y coordinate : " +y);
		GUI.Label ((new Rect (0, 80, 200, 100)), "Z coordinate : " +z);
		GUI.Label ((new Rect (0, 120, 200, 100)), "Distance : " +d);

	}
	void MoveToDestination(){
		this.agent.destination = goal; 
	}
}