## Date and time
17th August, 2015 : evening 7:30 pm.
Location: Reading Room, IIIT- Hyderabad

## Attendees
Client: Mr.Girish Sarwate.
Team members: Isha Mangurkar, Divanshu Jain, Natha Avinash, Thatavarthi Phanindra Kumar
TA Mentor: Sakshee Vijay

## Agenda
Introduction of the client and the team working on the project.
Getting a basic idea about the project.

## Minutes
### Discussion
*(What was discussed in the meeting and what were all the questions raised, by whom and what solutions suggested.)*
* Introducing all those people from the client side and the Team side.
 * Client gave contact details of himself and his team who could be reached in case of his absence.
 * A general background about the client, his company- DreamSlate and his educational background.
 * An introduction of the team members was given and the client asked about all the courses taken by each of the team members.
* A general outline of the project was given. 
 * Project was aimed at the application's ability to measure the length, width and height of a room/ a building etc. And returning the volume of that structure.
 * The project needed an integration of API's of Google Cardboard and SketchUp.
 * This idea about the project was vague and clearer picture could not be successfully obtained.
* Sakshee Ma'am asked about whether any image processing and higher level skills were necessary, as the client talked about measuring volume. Client denied this.
* Team members wanted to know more about API's and thus, requested the client to reply back with a clearer picture of the application.

### Action Points
*(Tasks identified to be completed by next meeting.)*
* A proper set of questions to be asked to the client was to be developed based on some background work.
 * Client said that he would come back with a clearer picture about what was desired in the project, in terms of inputs that shall be given, output expected, etc.
* Team members decided to do a little background study about API's and Google Cardboard API and Ruby SketchUp API.
* A skype call with the Client's consultant was to be fixed via email communication for further clarifications.

## Date of the next meeting
This date was to be fixed via Email Communication.
