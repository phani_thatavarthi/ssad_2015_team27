## Date and time
20th August, 2015

## Attendees
Skype meeting with
Client: Mr.Girish Sarwate, Client's Consultant: Mr.Akash Budhia
Team Members: Isha Mangurkar, Divanshu Jain, Natha Avinash, Thatavarthi Phanindra Kumar

## Agenda
A general introduction to the point of contact for technical doubts and an understanding about the project requirements.

## Minutes
### Discussion
*(whatever the clients has explained, what all doubts students asked, and solutions proposed.)*
* Client introduced Mr.Akash Budhia and a general introduction of all team members with the client was done.
* Mr. Akash asked about all the technologies we have already learnt/ worked with in the past. 
 * This was for him to get a general idea of what and how much have we had experience in application building.
* Mr.Akash also gave a brief idea about API's and integration of API's and asked us to do a reading up about the Google Cardboard API and SketchUp API.
* Further details were to be given via email communications/ Client's next meeting.

### Action Points
*(Tasks identified to be completed by next meeting.)*
* A little background study of the API's.
 * Google Cardboard API.
 * Ruby SketchUp API.
* Coming up with questions for the client before the next meeting.

## Date of the next meeting
Was to be decided via Email communication.
